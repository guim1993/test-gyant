# TEST GYANT #

This is an app that allows the doctor to review the EHR (one after the other) and label it with one of several conditions.

## What is in this repository? ###
A frontend application and a backend (API)

### How do I launch these applications? ###

The two applications run separately, and each has its own startup instructions.

### Frontend ###
Follow the instructions to launch the frontend by clicking [here](front/README.md)

### Server(API) ###
Follow the instructions to start the server by clicking [here](server/README.md)

