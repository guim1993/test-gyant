# Getting Started with Server

## Installation

### `npm install`
Install all api dependencies

## Running the app
Any of the modes will start the application on port 3005

### `npm start`
Initialize the api in development mode

### `npm run start:dev`
Initialize the api in development mode with watch

### `npm run start:prod`
Initialize the api in development mode


## Running tests

### `npm run test`
Run the unit tests

## Swagger
[http://localhost:3005/api](http://localhost:3005/api)

First use the authentication endpoint to redeem the token, then add the token using the authorize button in the upper right corner of the screen.

## Observations
The API is not using a local Mongo instance. It is configured to access the mongo cloud (Atlas).