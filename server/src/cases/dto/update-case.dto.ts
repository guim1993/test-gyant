import { User } from '../../users/interfaces/user.interface';
import { Condition } from '../../conditions/interfaces/condition.interface';

export class UpdateCaseDto {
  readonly description: string;
  readonly doctor: User;
  readonly condition: Condition;
  readonly updateAt: Date;
}
