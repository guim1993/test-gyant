export class CreateCaseDto {
  readonly description: string;
  readonly createAt: Date;
}
