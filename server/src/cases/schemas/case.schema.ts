import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from '../../users/schemas/user.schema';
import { Condition } from '../../conditions/schemas/condition.schema';

export type CaseDocument = Case & Document;

@Schema()
export class Case {
  @Prop()
  description: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  doctor: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Condition' })
  condition: Condition;

  @Prop({ default: new Date() })
  createAt: Date;

  @Prop()
  updateAt: Date;
}

export const CaseSchema = SchemaFactory.createForClass(Case);
