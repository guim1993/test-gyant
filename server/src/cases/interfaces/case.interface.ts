import { Document } from 'mongoose';
import { User } from '../../users/interfaces/user.interface';
import { Condition } from '../../conditions/interfaces/condition.interface';

export interface Case extends Document {
  readonly description: string;
  readonly doctor: User;
  readonly condition: Condition;
  readonly createAt: Date;
  readonly updateAt: Date;
}
