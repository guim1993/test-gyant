import { Connection } from 'mongoose';
import { CaseSchema } from './schemas/case.schema';

export const casesProviders = [
  {
    provide: 'Case_MODEL',
    useFactory: (connection: Connection) => connection.model('Case', CaseSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
