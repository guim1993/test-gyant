import { Test, TestingModule } from '@nestjs/testing';
import { CasesController } from '../cases.controller';
import { CasesService } from '../cases.service';
import { CreateCaseDto } from '../dto/create-case.dto';
import { UpdateCaseDto } from '../dto/update-case.dto';

describe('CasesController', () => {
  let casesController: CasesController;
  let spyService: CasesService;
  let res;
  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: CasesService,
      useFactory: () => ({
        create: jest.fn(() => []),
        update: jest.fn(() => []),
        findAll: jest.fn(() => []),
        findToLabel: jest.fn(() => []),
        findOne: jest.fn(() => []),
        delete: jest.fn(() => []),
      }),
    };
    const app: TestingModule = await Test.createTestingModule({
      controllers: [CasesController],
      providers: [CasesService, ApiServiceProvider],
    }).compile();

    casesController = app.get<CasesController>(CasesController);
    spyService = app.get<CasesService>(CasesService);
    res = {
      status: (status) => ({
        send: (response) => [],
      }),
    };
  });

  it('calling create case method', () => {
    const dto = new CreateCaseDto();
    expect(casesController.create(dto)).not.toEqual(null);
  });

  it('calling update case method', () => {
    const dto = new UpdateCaseDto();
    expect(casesController.update('id', dto)).not.toEqual(null);
  });

  it('calling findAll cases method', () => {
    expect(casesController.findAll()).not.toEqual(null);
  });

  it('calling findOne case method', () => {
    expect(casesController.findOne('id')).not.toEqual(null);
  });

  it('calling findToLabel case method', () => {
    expect(casesController.findToLabel(res)).not.toEqual(null);
  });

  it('calling delete case method', () => {
    expect(casesController.delete('id')).not.toEqual(null);
  });
});
