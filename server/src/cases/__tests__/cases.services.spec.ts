import { Test, TestingModule } from '@nestjs/testing';
import { CasesService } from '../cases.service';
import { CreateCaseDto } from '../dto/create-case.dto';
import { UpdateCaseDto } from '../dto/update-case.dto';

class ApiServiceMock {
  create(dto: any) {
    return {};
  }
  update(id: string, dto: any) {
    return {};
  }
  findAll() {
    return [];
  }
  findToLabel() {
    return [];
  }
  findOne() {
    return {};
  }
  delete(id: string) {
    return {};
  }
}
describe.only('CasesService', () => {
  let casesService: CasesService;

  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: CasesService,
      useClass: ApiServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [CasesService, ApiServiceProvider],
    }).compile();
    casesService = module.get<CasesService>(CasesService);
  });

  it('should call create method with expected params', async () => {
    const createCasesSpy = jest.spyOn(casesService, 'create');
    const dto = new CreateCaseDto();
    casesService.create(dto);
    expect(createCasesSpy).toHaveBeenCalledWith(dto);
  });

  it('should call update method with expected params', async () => {
    const updateCasesSpy = jest.spyOn(casesService, 'update');
    const dto = new UpdateCaseDto();
    casesService.update('id', dto);
    expect(updateCasesSpy).toHaveBeenCalledWith('id', dto);
  });

  it('should call findAll method', async () => {
    const findAllCasesSpy = jest.spyOn(casesService, 'findAll');
    casesService.findAll();
    expect(findAllCasesSpy).toHaveBeenCalled();
  });

  it('should call findToLabel method', async () => {
    const findToLabelCasesSpy = jest.spyOn(casesService, 'findToLabel');
    casesService.findToLabel();
    expect(findToLabelCasesSpy).toHaveBeenCalled();
  });

  it('should call findOne method with expected params', async () => {
    const findOneCasesSpy = jest.spyOn(casesService, 'findOne');
    casesService.findOne('id');
    expect(findOneCasesSpy).toHaveBeenCalledWith('id');
  });

  it('should call delete method with expected params', async () => {
    const deleteCasesSpy = jest.spyOn(casesService, 'delete');
    casesService.delete('id');
    expect(deleteCasesSpy).toHaveBeenCalledWith('id');
  });
});
