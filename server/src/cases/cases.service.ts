import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateCaseDto } from './dto/create-case.dto';
import { UpdateCaseDto } from './dto/update-case.dto';
import { Case } from './interfaces/case.interface';

@Injectable()
export class CasesService {
  constructor(@Inject('Case_MODEL') private readonly caseModel: Model<Case>) {}

  async create(createCaseDto: CreateCaseDto): Promise<Case> {
    const createdCase = new this.caseModel(createCaseDto);
    return createdCase.save();
  }

  async update(id, updateCaseDto: UpdateCaseDto): Promise<Case> {
    return this.caseModel
      .findByIdAndUpdate({ _id: id }, updateCaseDto, { new: true })
      .populate('doctor')
      .populate('condition');
  }

  async findAll(): Promise<Case[]> {
    return this.caseModel
      .find()
      .populate('doctor')
      .populate('condition')
      .exec();
  }

  async findToLabel(): Promise<Case[]> {
    return this.caseModel
      .find({ doctor: { $exists: false } })
      .limit(1)
      .exec();
  }

  async findOne(id): Promise<Case> {
    return this.caseModel
      .findById(id)
      .populate('doctor')
      .populate('condition')
      .exec();
  }

  async delete(id): Promise<any> {
    return this.caseModel.findByIdAndRemove(id);
  }
}
