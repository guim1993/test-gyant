import {
  Controller,
  Get,
  Post,
  Patch,
  Body,
  Delete,
  Param,
  Res,
} from '@nestjs/common';
import { ApiBody, ApiBearerAuth, ApiTags, ApiParam } from '@nestjs/swagger';
import { CreateCaseDto } from './dto/create-case.dto';
import { UpdateCaseDto } from './dto/update-case.dto';
import { CasesService } from './cases.service';
import { Case } from './interfaces/case.interface';

@Controller('cases')
@ApiTags('Cases')
@ApiBearerAuth('token')
export class CasesController {
  constructor(private readonly casesService: CasesService) {}

  @Post()
  @ApiBody({
    schema: {
      example: {
        description: 'Case Description',
      },
    },
  })
  async create(@Body() createCaseDto: CreateCaseDto) {
    return this.casesService.create(createCaseDto);
  }

  @Patch('/:id')
  @ApiBody({
    schema: {
      example: {
        doctor: 'Doctor/User id',
        condition: 'Condition id',
        updateAt: new Date(),
      },
    },
  })
  @ApiParam({ name: 'id', type: 'string' })
  async update(@Param('id') id, @Body() updateCaseDto: UpdateCaseDto) {
    const params = { ...updateCaseDto, updateAt: new Date() };
    return this.casesService.update(id, params);
  }

  @Get()
  async findAll(): Promise<Case[]> {
    return this.casesService.findAll();
  }

  @Get('/tolabel')
  async findToLabel(@Res() res): Promise<Case | {}> {
    const cases = await this.casesService.findToLabel();
    if (cases.length > 0) {
      return res.status(200).send(cases[0]);
    } else {
      return res.status(204).send();
    }
  }

  @Get('/:id')
  @ApiParam({ name: 'id', type: 'string' })
  async findOne(@Param('id') id): Promise<Case> {
    return this.casesService.findOne(id);
  }

  @Delete('/:id')
  @ApiParam({ name: 'id', type: 'string' })
  async delete(@Param('id') id): Promise<Case> {
    return this.casesService.delete(id);
  }
}
