import { Module } from '@nestjs/common';
import { CasesController } from './cases.controller';
import { CasesService } from './cases.service';
import { casesProviders } from './cases.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [CasesController],
  providers: [CasesService, ...casesProviders],
})
export class CasesModule {}
