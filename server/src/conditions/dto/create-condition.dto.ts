export class CreateConditionDto {
  readonly code: string;
  readonly description: string;
}
