import { Module } from '@nestjs/common';
import { ConditionsController } from './conditions.controller';
import { ConditionsService } from './conditions.service';
import { conditionsProviders } from './conditions.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [ConditionsController],
  providers: [ConditionsService, ...conditionsProviders],
  exports: [ConditionsService],
})
export class ConditionsModule {}
