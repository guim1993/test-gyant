import { Document } from 'mongoose';

export interface Condition extends Document {
  readonly code: string;
  readonly description: string;
}
