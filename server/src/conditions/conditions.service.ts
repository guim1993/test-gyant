import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateConditionDto } from './dto/create-condition.dto';
import { Condition } from './interfaces/condition.interface';

@Injectable()
export class ConditionsService {
  constructor(@Inject('Conditions_MODEL') private readonly conditionModel: Model<Condition>) {}

  async create(createConditionDto: CreateConditionDto): Promise<Condition> {
    const createdCondition = new this.conditionModel(createConditionDto);
    return createdCondition.save();
  }

  async findAll(): Promise<Condition[]> {
    return this.conditionModel.find().exec();
  }

  async findOne(id): Promise<Condition> {
    return this.conditionModel.findById(id).exec();
  }

  async delete(id): Promise<any> {
    return this.conditionModel.findByIdAndRemove(id);
  }
}
