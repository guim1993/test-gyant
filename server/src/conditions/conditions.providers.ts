import { Connection } from 'mongoose';
import { ConditionSchema } from './schemas/condition.schema';

export const conditionsProviders = [
  {
    provide: 'Conditions_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('Condition', ConditionSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
