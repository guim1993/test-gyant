import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { ApiBody, ApiBearerAuth, ApiTags, ApiParam } from '@nestjs/swagger';
import { CreateConditionDto } from './dto/create-condition.dto';
import { ConditionsService } from './conditions.service';
import { Condition } from './interfaces/condition.interface';

@Controller('conditions')
@ApiTags('Conditions')
@ApiBearerAuth('token')
export class ConditionsController {
  constructor(private readonly conditionsService: ConditionsService) {}

  @Post()
  @ApiBody({
    schema: {
      example: {
        code: 'Code Name',
        description: 'Code Description',
      },
    },
  })
  async create(@Body() createConditionDto: CreateConditionDto) {
    this.conditionsService.create(createConditionDto);
  }

  @Get()
  async findAll(): Promise<Condition[]> {
    return this.conditionsService.findAll();
  }

  @Get('/:id')
  @ApiParam({ name: 'id', type: 'string' })
  async findOne(@Param('id') id): Promise<Condition> {
    return this.conditionsService.findOne(id);
  }

  @Delete('/:id')
  @ApiParam({ name: 'id', type: 'string' })
  async delete(@Param('id') id): Promise<Condition> {
    return this.conditionsService.delete(id);
  }
}
