import { Controller, Request, Post, Get, UseGuards } from '@nestjs/common';
import { ApiBody, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { Public } from '../decorators/skipAuth';

@Controller('auth')
@ApiTags('Auth')
@ApiBearerAuth('token')
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UsersService,
  ) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @Post()
  @ApiBody({
    schema: {
      example: {
        email: 'teste@teste.com',
        password: 'admin',
      },
    },
  })
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Get('profile')
  @ApiBearerAuth()
  async getProfile(@Request() req) {
    return this.userService.findOneByEmail(req.user.email);
  }
}
