import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { ApiBody, ApiBearerAuth, ApiTags, ApiParam } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { User } from './interfaces/user.interface';

@Controller('user')
@ApiTags('Users')
@ApiBearerAuth('token')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @ApiBody({
    schema: {
      example: {
        name: 'User name',
        email: 'User email',
        password: 'password',
      },
    },
  })
  async create(@Body() createUserDto: CreateUserDto) {
    this.usersService.create(createUserDto);
  }

  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Get('/:id')
  @ApiParam({ name: 'id', type: 'string' })
  async findOne(@Param('id') id): Promise<User> {
    return this.usersService.findOne(id);
  }

  @Delete('/:id')
  @ApiParam({ name: 'id', type: 'string' })
  async delete(@Param('id') id): Promise<User> {
    return this.usersService.delete(id);
  }
}
