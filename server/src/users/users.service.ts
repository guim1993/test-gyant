import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(@Inject('User_MODEL') private readonly userModel: Model<User>) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async findOne(id): Promise<User> {
    return this.userModel.findById(id).exec();
  }

  async findOneByEmail(email): Promise<User> {
    return this.userModel.findOne({ email: email }).exec();
  }

  async delete(id): Promise<User> {
    return this.userModel.findByIdAndRemove(id);
  }
}
