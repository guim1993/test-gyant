import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect('mongodb+srv://test-gyant:test-gyant@test-gyant.dcaf5.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'),
  },
];
