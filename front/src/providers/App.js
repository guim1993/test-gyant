import { createContext, useContext } from 'react';
import { auth, getLoggedUser } from '../services/Auth';

const AppContext = createContext();

export function Provider({ children, isLoading, setIsLoading, user, setUser }) {

  const doLogin = (data) => {
    return auth(data).then(response => {
      if (response.data) {
        localStorage.setItem('token', response.data.access_token);
        return getLoggedUser();
      }

      return response;
    }).then(result => {
      if (result.data) {
        setUser(result.data);
      }

      return result.data;
    });
  }

  const doLogout = () => {
    localStorage.removeItem('token');
    setUser(undefined);
    window.location.href = "/"
  }

  return (
    <AppContext.Provider
      value={{
        isLoading,
        setIsLoading,
        doLogin,
        doLogout,
        user
      }}
    >
      {children}
    </AppContext.Provider>
  );
}

export default function useAppContext() {
  return useContext(AppContext);
}
