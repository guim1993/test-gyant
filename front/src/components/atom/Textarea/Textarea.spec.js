import { render, fireEvent, screen } from "@testing-library/react";
import Textarea from "./Textarea";

it("Textarea tests", () => {
  const onChange = jest.fn();
  const { container } = render(
    <Textarea
      name="text"
      changeValue={onChange}
      value=""
      hasError={false}
      placeholder="Text"
    />
  );

  const input = screen.getByRole("textbox");
  expect(input).toBeInTheDocument();

  fireEvent.change(input, { target: { value: "23" } });
  expect(onChange).toBeCalled();

  expect(container).toMatchSnapshot();
});
