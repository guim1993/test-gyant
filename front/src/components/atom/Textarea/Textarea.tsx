import styles from "./Textarea.module.scss";

interface TextareaProps {
  name: string;
  value: string;
  changeValue?: React.ChangeEventHandler<HTMLTextAreaElement>;
  placeholder?: string;
  hasError?: boolean;
}

function Textarea({
  name,
  value,
  changeValue,
  placeholder = "",
  hasError,
}: TextareaProps) {
  return (
    <textarea
      name={name}
      value={value}
      onChange={changeValue}
      placeholder={placeholder}
      className={`${styles.textarea} ${hasError ? styles.hasError : ""}`}
    />
  );
}

export default Textarea;
