import styles from "./Select.module.scss";

interface SelectProps {
  name: string;
  options: {
    [key: string]: string;
  }[];
  value: string;
  nameIndex?: string;
  valueIndex?: string;
  changeValue: React.ChangeEventHandler<HTMLSelectElement>;
  placeholder?: string;
  size?: number;
  hasError?: boolean;
}

function Select({
  name,
  options = [],
  value,
  nameIndex = "name",
  valueIndex = "id",
  changeValue,
  placeholder = "",
  size = 1,
  hasError,
}: SelectProps) {
  return (
    <select
      name={name}
      value={value}
      onChange={changeValue}
      className={`${styles.select} ${hasError ? styles.hasError : ""}`}
      size={size}
    >
      {placeholder && (
        <option value="" key="0">
          {placeholder}
        </option>
      )}
      {options.map((option, index) => {
        return (
          <option value={option[valueIndex]} key={`option-${index}`}>
            {option[nameIndex]}
          </option>
        );
      })}
    </select>
  );
}

export default Select;
