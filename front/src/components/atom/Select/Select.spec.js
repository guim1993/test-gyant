import { render, fireEvent, screen } from "@testing-library/react";
import Select from "./Select";

it("Select tests", () => {
  const onChange = jest.fn();
  const { container } = render(
    <Select
      name="condition"
      options={[
        {
          id: "1",
          name: "Option 1",
        },
        {
          id: "2",
          name: "Option 2",
        },
      ]}
      changeValue={onChange}
      value=""
      size={11}
      hasError={false}
    />
  );

  const select = screen.getByRole("listbox");
  expect(select).toBeInTheDocument();

  fireEvent.change(select, { target: { value: "2" } });
  expect(onChange).toBeCalled();

  expect(container).toMatchSnapshot();
});
