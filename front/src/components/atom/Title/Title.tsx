import styles from "./Title.module.scss";

interface TitleProps {
  children: React.ReactNode;
}

export default function Title({ children }: TitleProps) {
  return <div className={styles.title}>{children}</div>;
}
