import { render } from "@testing-library/react";
import Title from "./Title";

it("Title tests", () => {
  const { container } = render(
    <Title>Title</Title>
  );
  expect(container).toMatchSnapshot();
});
