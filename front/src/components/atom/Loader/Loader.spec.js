import { render } from "@testing-library/react";
import Loader from "./Loader";

it("Loader tests", () => {
  const { container } = render(
    <Loader />
  );
  expect(container).toMatchSnapshot();
});
