import { NavLink } from "react-router-dom";
import styles from "./Link.module.scss";

interface LinkProps {
  children: React.ReactNode;
  to: string;
}
function Link({ children, to }: LinkProps) {
  return (
    <NavLink to={to} className={styles.link}>
      {children}
    </NavLink>
  );
}

export default Link;
