import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Link from "./Link";

it("Link tests", () => {
  const { container } = render(
    <BrowserRouter>
      <Link to="/">Teste</Link>
    </BrowserRouter>
  );
  expect(container).toMatchSnapshot();
});
