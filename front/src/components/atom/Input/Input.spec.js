import { render, fireEvent, screen } from "@testing-library/react";
import Input from "./Input";

it("Input tests", () => {
  const onChange = jest.fn();
  const { container } = render(
    <Input
      name="input"
      changeValue={onChange}
      value=""
      placeholder="Input"
      hasError={false}
    />
  );

  const input = screen.getByRole("textbox");
  expect(input).toBeInTheDocument();

  fireEvent.change(input, { target: { value: "23" } });
  expect(onChange).toBeCalled();

  expect(container).toMatchSnapshot();
});
