import styles from "./Input.module.scss";

interface InputProps {
  name: string;
  type?: string;
  value: string;
  changeValue: React.ChangeEventHandler<HTMLInputElement>;
  placeholder?: string;
  hasError?: boolean;
}

function Input({
  name,
  type = "text",
  value,
  changeValue,
  placeholder = "",
  hasError,
}: InputProps) {
  return (
    <input
      name={name}
      type={type}
      value={value}
      onChange={changeValue}
      placeholder={placeholder}
      className={`${styles.input} ${hasError ? styles.hasError : ""}`}
    />
  );
}

export default Input;
