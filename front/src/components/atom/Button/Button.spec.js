import { render, fireEvent, screen } from "@testing-library/react";
import Button from "./Button";

it("Button tests", () => {
  const onClick = jest.fn();
  const { container } = render(<Button onClick={onClick}>Teste</Button>);

  const button = screen.getByText("Teste");
  expect(button).toBeInTheDocument();

  fireEvent.click(button);
  expect(onClick).toBeCalled();

  expect(container).toMatchSnapshot();
});
