import styles from "./Button.module.scss";

interface ButtonProps {
  onClick?: () => void;
  children: React.ReactNode;
  type?: "button" | "link";
}
function Button({ onClick, type = "button", children }: ButtonProps) {
  const types: { [key: string]: string } = {
    button: styles.button,
    link: styles.link,
  };
  return (
    <button onClick={onClick} className={types[type]}>
      {children}
    </button>
  );
}

export default Button;
