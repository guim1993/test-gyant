import styles from "./Menu.module.scss";
import Link from "../../atom/Link/Link";
import Button from "../../atom/Button/Button";

interface MenuType {
  link?: string;
  onClick?: () => void;
  label: string;
}

interface MenuPropsType {
  links: MenuType[];
}
function Menu({ links }: MenuPropsType) {
  return (
    <nav className={styles.menu}>
      <ul className={styles.menuList}>
        {links.map((menu, index) => (
          <li key={`menu-${index}`} className={styles.menuItem}>
            {menu.link && <Link to={menu.link}>{menu.label}</Link>}
            {menu.onClick && (
              <Button onClick={menu.onClick} type="link">
                {menu.label}
              </Button>
            )}
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default Menu;
