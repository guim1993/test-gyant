import { FormEvent } from "react";
import Title from "../../atom/Title/Title";
import Textarea from "../../atom/Textarea/Textarea";
import styles from "./Case.module.scss";

interface CaseProps {
  description: string;
  handleUpdate?: (e: FormEvent<HTMLTextAreaElement>) => void;
  hasError?: boolean;
}

export default function Case({
  description,
  handleUpdate,
  hasError,
}: CaseProps) {
  return (
    <div className={styles.case}>
      <Title>Please review this case:</Title>
      <Textarea
        name="description"
        changeValue={(e: FormEvent<HTMLTextAreaElement>) => handleUpdate?.(e)}
        value={description}
        hasError={hasError}
      />
    </div>
  );
}
