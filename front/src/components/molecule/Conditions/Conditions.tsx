import { FormEvent } from "react";
import Title from "../../atom/Title/Title";
import styles from "./Conditions.module.scss";
import Select from "../../atom/Select/Select";

interface ConditionsProps {
  options: {
    [key: string]: string;
  }[];
  selected: string;
  handleUpdate?: (e: FormEvent<HTMLSelectElement>) => void;
  hasError?: boolean;
}

export default function Conditions({
  options,
  selected,
  handleUpdate,
  hasError,
}: ConditionsProps) {
  return (
    <div className={styles.case}>
      <Title>Select Condition:</Title>
      <Select
        name="condition"
        options={options}
        changeValue={(e: FormEvent<HTMLSelectElement>) => handleUpdate?.(e)}
        value={selected}
        size={11}
        hasError={hasError}
      />
    </div>
  );
}
