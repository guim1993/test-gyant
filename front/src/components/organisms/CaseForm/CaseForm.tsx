import { Dispatch, SetStateAction, FormEvent } from "react";
import Case from "../../molecule/Case/Case";
import Conditions from "../../molecule/Conditions/Conditions";
import Button from "../../atom/Button/Button";

import { updateForm } from "../../../utils/handleForm";

import { CaseType, ConditionsType } from "../../../types";

import styles from "./CaseForm.module.scss";

interface CaseFormProps {
  conditions: ConditionsType[];
  formData: {
    [key: string]: string;
  };
  setFormData: Dispatch<
    SetStateAction<{
      description: string;
      condition: string;
    }>
  >;
  handleSubmit: (e: FormEvent<HTMLFormElement>) => void;
  errors: {
    [key: string]: boolean;
  };
  errorMessage: string;
}

export default function CaseForm({
  conditions,
  handleSubmit,
  formData,
  setFormData,
  errorMessage,
  errors,
}: CaseFormProps) {
  const options: { [key: string]: string }[] = conditions.map((condition) => ({
    id: condition._id,
    name: `${condition.description} (${condition.code})`,
  }));

  return (
    <form onSubmit={handleSubmit} className={styles.caseForm}>
      <div className={styles.case}>
        <Case
          description={formData.description}
          handleUpdate={(e: FormEvent<HTMLTextAreaElement>) =>
            updateForm(e, formData, setFormData)
          }
          hasError={!!errors.description}
        />
      </div>
      <div className={styles.conditions}>
        <Conditions
          options={options}
          selected={formData.condition}
          handleUpdate={(e: FormEvent<HTMLSelectElement>) =>
            updateForm(e, formData, setFormData)
          }
          hasError={!!errors.condition}
        />
        <p className={styles.error}>{errorMessage}</p>
        <Button>Next Case</Button>
      </div>
    </form>
  );
}
