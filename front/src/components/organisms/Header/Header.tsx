import useAppContext from "../../../providers/App";

import Menu from "../../molecule/Menu/Menu";

import styles from "./Header.module.scss";

interface HeaderProps {
  children?: React.ReactNode;
}
function Header({ children }: HeaderProps) {
  const { user, doLogout } = useAppContext();

  const links = [
    {
      label: user.name,
      link: "/",
    },
    {
      label: "Log Out",
      onClick: doLogout,
    },
  ];
  return (
    <header className={styles.header}>
      {children}
      <Menu links={links} />
    </header>
  );
}

export default Header;
