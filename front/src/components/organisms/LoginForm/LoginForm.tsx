import { Dispatch, SetStateAction, FormEvent } from "react";
import { updateForm } from "../../../utils/handleForm";

import Input from "../../atom/Input/Input";
import Button from "../../atom/Button/Button";
import styles from "./LoginForm.module.scss";

interface LoginFormProps {
  formData: {
    [key: string]: string;
  };
  setFormData: Dispatch<
    SetStateAction<{
      email: string;
      password: string;
    }>
  >;
  handleSubmit: (e: FormEvent<HTMLFormElement>) => void;
  errors: {
    [key: string]: boolean;
  };
  errorMessage: string;
}

function LoginForm({
  handleSubmit,
  formData,
  setFormData,
  errorMessage,
  errors,
}: LoginFormProps) {
  return (
    <form onSubmit={handleSubmit} className={styles.loginForm}>
      <Input
        name="email"
        changeValue={(e: FormEvent<HTMLInputElement>) =>
          updateForm(e, formData, setFormData)
        }
        value={formData.email}
        placeholder="Email"
        hasError={!!errors.email}
      />

      <Input
        name="password"
        type="password"
        changeValue={(e: FormEvent<HTMLInputElement>) =>
          updateForm(e, formData, setFormData)
        }
        value={formData.password}
        placeholder="Password"
        hasError={!!errors.password}
      />

      <p className={styles.error}>{errorMessage}</p>

      <Button>Login</Button>
    </form>
  );
}

export default LoginForm;
