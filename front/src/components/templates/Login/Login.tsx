import styles from "./Login.module.scss";

interface LoginProps {
  children?: React.ReactNode;
}
function LoginTemplate({ children }: LoginProps) {
  return (
    <div className={styles.loginTemplate}>
      <main className={styles.content}>{children}</main>
    </div>
  );
}

export default LoginTemplate;
