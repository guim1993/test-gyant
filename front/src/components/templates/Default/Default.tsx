import styles from "./Default.module.scss";

interface DefaultProps {
  header?: React.ReactNode;
  footer?: React.ReactNode;
  children?: React.ReactNode;
}
function DefaultTemplate({ header, footer, children }: DefaultProps) {
  return (
    <div className={styles.defaultTemplate}>
      {header && header}
      <main className={styles.content}>{children}</main>
      {footer && footer}
    </div>
  );
}

export default DefaultTemplate;
