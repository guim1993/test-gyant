export interface User {
  id: string;
  name: string;
  email: string;
}

export interface CaseType {
  _id: string;
  description: string;
}

export interface ConditionsType {
  _id: string;
  code: string;
  description: string;
}
