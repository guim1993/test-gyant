import axios from "axios";

const apiUrl = "http://localhost:3005/";

let instance = axios.create({
  baseURL: apiUrl,
  responseType: "json",
});

instance.interceptors.request.use(function (config) {
  const token = localStorage.getItem("token");
  config.headers.Authorization = token ? `Bearer ${token}` : "";
  return config;
});

instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    localStorage.removeItem("token");
    window.location.href = "/";
    return Promise.reject(error);
  }
);

export default instance;
