import Api from './Api';

export const getCasesToLabel = () => {
  return Api.get(`/cases/tolabel`);
}

export const updateCase = (id, body) => {
  return Api.patch(`/cases/${id}`, body);
}