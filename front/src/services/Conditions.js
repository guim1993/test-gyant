import Api from './Api';

export const getConditions = () => {
  return Api.get(`/conditions`);
}