import Api from './Api';

export const auth = (user) => {
  return Api.post(`/auth`, user);
}

export const getLoggedUser = () => {
  return Api.get(`/auth/profile`);
}