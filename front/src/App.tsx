import { useState, useEffect } from "react";
import { Routes, Route, BrowserRouter, Navigate } from "react-router-dom";

import { User } from "./types";

import { Provider as AppContextProvider } from "./providers/App";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Loader from "./components/atom/Loader/Loader";

import { getLoggedUser } from "./services/Auth";

function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState<User | undefined>(undefined);

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      getLoggedUser()
        .then((result: any) => {
          if (result.data) {
            setUser(result.data);
          }
          setIsLoading(false);
        })
        .catch(() => {
          localStorage.removeItem("token");
          setIsLoading(false);
        });
    } else {
      setIsLoading(false);
    }
  }, []);

  return (
    <BrowserRouter>
      <AppContextProvider
        user={user}
        setUser={setUser}
        isLoading={isLoading}
        setIsLoading={setIsLoading}
      >
        {!user && !isLoading && <Login />}

        {user && (
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        )}

        {isLoading && <Loader />}
      </AppContextProvider>
    </BrowserRouter>
  );
}

export default App;
