import { useState, useEffect } from "react";

import useAppContext from "../providers/App";

import { validateForm, changeErrors } from "../utils/handleForm";

import LoginTemplate from "../components/templates/Login/Login";
import LoginForm from "../components/organisms/LoginForm/LoginForm";

interface FormErrors {
  email: boolean;
  password: boolean;
}

function Login() {
  const { doLogin } = useAppContext();
  const initialValues = {
    email: "teste@teste.com",
    password: "admin",
  };
  const [formData, setFormData] = useState(initialValues);
  const [errors, setErrors] = useState({
    email: false,
    password: false,
  });
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const checkErrors = validateForm(errors, formData);
    setErrors(checkErrors.errors as FormErrors);
    if (checkErrors.hasError) return;

    doLogin(formData).catch((error: any) => {
      if (error.response.data) {
        setErrorMessage(error.response.data.message);
      }
    });
  };

  useEffect(() => {
    const updatedErrors = changeErrors(errors, formData);
    setErrors(updatedErrors);
  }, [formData]);

  return (
    <LoginTemplate>
      <LoginForm
        handleSubmit={handleSubmit}
        formData={formData}
        setFormData={setFormData}
        errors={errors}
        errorMessage={errorMessage}
      />
    </LoginTemplate>
  );
}

export default Login;
