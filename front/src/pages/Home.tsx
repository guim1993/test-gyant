import { useEffect, useState } from "react";

import useAppContext from "../providers/App";

import DefaultTemplate from "../components/templates/Default/Default";
import Header from "../components/organisms/Header/Header";
import Footer from "../components/organisms/Footer/Footer";
import CaseForm from "../components/organisms/CaseForm/CaseForm";

import { CaseType, ConditionsType } from "../types";

import { getCasesToLabel, updateCase } from "../services/Cases";
import { getConditions } from "../services/Conditions";

import { validateForm, changeErrors } from "../utils/handleForm";
import Title from "../components/atom/Title/Title";

interface FormErrors {
  description: boolean;
  condition: boolean;
}

function Home() {
  const { user, isLoading, setIsLoading } = useAppContext();
  const [conditions, setConditions] = useState<ConditionsType[]>([]);
  const [caseToLabel, setCaseToLabel] = useState<CaseType | "">();

  const initialValues = {
    description: "",
    condition: "",
  };
  const [formData, setFormData] = useState(initialValues);
  const [errors, setErrors] = useState({
    description: false,
    condition: false,
  });
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const checkErrors = validateForm(errors, formData);
    setErrors(checkErrors.errors as FormErrors);
    if (checkErrors.hasError || !caseToLabel) return;

    updateCase(caseToLabel._id, { ...formData, doctor: user._id })
      .then((response) => {
        if (response.data) {
          getNextCaseToLabel();
        }
      })
      .catch((error: any) => {
        if (!error.data) {
          setErrorMessage("Error");
        }
      });
  };

  const getNextCaseToLabel = () => {
    setIsLoading(true);
    getCasesToLabel().then((response) => {
      const { data, status } = response;

      setCaseToLabel(data);
      if (status === 200) {
        setFormData({ ...initialValues, description: data?.description });
      }
    });
  };

  useEffect(() => {
    setIsLoading(true);
    getNextCaseToLabel();

    getConditions().then((response) => {
      const { data = [] } = response;
      setConditions(data);
    });
  }, []);

  useEffect(() => {
    const updatedErrors = changeErrors(errors, formData);
    setErrors(updatedErrors);

    if (isLoading && conditions.length) {
      setIsLoading(false);
    }
  }, [formData, conditions, caseToLabel]);

  return (
    <DefaultTemplate header={<Header>Brand</Header>} footer={<Footer />}>
      <div className="container">
        {!!conditions.length && caseToLabel && !isLoading && (
          <CaseForm
            conditions={conditions}
            handleSubmit={handleSubmit}
            formData={formData}
            setFormData={setFormData}
            errors={errors}
            errorMessage={errorMessage}
          />
        )}
        {!!conditions.length && !caseToLabel && !isLoading && (
          <Title>You are Done</Title>
        )}
      </div>
    </DefaultTemplate>
  );
}

export default Home;
