# Getting Started with Front

## Installation

### `npm install`
Install all front dependencies

## Running the app
### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

If port 3000 is already in use, you will be asked in the terminal if you want to run on another port.

If this is the case, use the new port to access in the browser


## Running tests
### `npm test`
Test project components (currently only atoms)

## Production build
### `npm run build`
Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.
